# Learning Rust

This is only a personal dummy project for learning purposes.

## Declaring...

Some useful links:
* [See scalar types](https://doc.rust-lang.org/book/second-edition/ch03-02-data-types.html)
* Notes about [str vs String](http://www.ameyalokare.com/rust/2017/10/12/rust-str-vs-String.html) and [why you cannot return a borrowed String struct attribute](https://stackoverflow.com/a/40075101/840635). Also how to define a struct `&str` attribute [using named lifetime notation](https://stackoverflow.com/a/26360487/840635). Read more in [Chapter 4](https://doc.rust-lang.org/book/second-edition/ch04-03-slices.html).

_Rust code uses snake case as the conventional style for function and variable names_ :disappointed:

Declare var:

```rust
let name: string = String::new()
```

Declare a mutable var:

```rust
let mut counter: i16 = 0;
```

Declare a constant:

```rust
const MAX_VALUE = 5000;
```
A constant may be declared outside of functions.

Declare a boolean:
    
    let valid: bool = false;

Char type uses single quotes while strings uses double quotes:
```rust
fn main() {
    let license_type: char = 'A';
    
    let license_number: &str = "33872CBF-92";
    
    println!("License type: {}  license number: {}", license_type, license_number);
}
```    

Declare a tuple and destructuring:
```rust
fn main() {
    let warrior: (&str, &str, u8) = ("Legolas", "elf", 18i8);
    
    let (name, race, level) = warrior;
    println!("Warrior's name: {}, race: {}, level: {}", name, race, level);
    
    // you can also reference tuple values by index
    println!("Warrior's name: {}, race: {}, level: {}", warrior.0, warrior.1, warrior.2);
}
```

In arrays all values must be of the same type. They have fixed size.
Vectors are allowed to grow and shrink.
```rust
fn main() {
    let fruits = ["apple", "orange"];
    
    println!("First fruit: {}, second fruit: {}", fruits[0], fruits[1]);   
}
```

Functions may be declared in any order:
```rust
fn main() {
    say_hello("Ragnar", 6);
}

fn say_hello(name: &str, unread_messages: u8) {
    println!("Hello {}, you have {} unread messages", name, unread_messages);
}
```

[Read about difference between sentences and expressions](https://doc.rust-lang.org/book/second-edition/ch03-03-how-functions-work.html#function-bodies-contain-statements-and-expressions)

You can use an expression to assign a value. An expression can be enclosed by brackets which defines an scope
and has a final expression with no semicolon at the end.
```rust
fn main() {
    let x = 5;
    
    let eleven = {
        let x = 2*x;
        x+1
    };
    
    println!("{} x 2 + 1 = {}", x, eleven);
}
```

The return value of a function is the value of the last expression of the function body. 
You can also return a value using the keyword `return`.

The type of a function is declared with an arrow `->` .
```rust
use std::string::String;

fn hello(name: &str) -> String  {
    let verb = "Hello";
    format!("{} {}", verb, name)
}

fn main() {
    let greeting = hello("Adam");

    println!("{}! Come in!", greeting);
}
```

You can shadow a variable by adding a new let. This allow you to apply transformations without mutate the original variable.
This is because, in fact, you are creating a new variable instead of mutating the original one.
```rust
fn main() {
    let x = 5;

    let x = x + 1;

    let x = x * 2;

    println!("The shadowed value of x is now: {}", x);
}
```

You can build a ternary operator using an if-else structure with expressions:
```rust
fn main() {
    let x = 5;

    let s = if x = 5 { "five" } else { "other" };
}
```

You can exit from a `loop` or a `while` with `break` keyword.

You can iterate an iterator with a `for`:
```rust
fn main() {
   let nums = ["one", "two", "three"];
   
   for num in nums.iter() {
       println!("the number is: {}", num);
   }
}
```

You can also use a `for` for iterate over an integer range expressed like `(min..max)`. You an also reverse the
range calling the `.rev()` function to the range:
```rust
fn main() {
   println!("Countdown from 10 to 1...");
   
   for number in (1..11).rev(){
    println!("... {}", number);
   }
   
   println!("launch!");
}
```

https://doc.rust-lang.org/book/second-edition/ch04-01-what-is-ownership.html#ownership-and-functions

## Custom errors
You can use [quick-error](https://stackoverflow.com/a/42584607/840635) crate to define them as enums.

## Equivalent code comparing with javascript

If at least one element meets a condition.

```javascript
const hasElementsGreaterThan10 = [1,3,7,10,15].some( x => x > 10);
```

```rust
fn main() {
    let has_elements_greater_than_10 = [1,3,7,10,15].iter().any(|&x| x > 10);

    println!("has elements greater than 10: {}", has_elements_greater_than_10);
}
```

## Test

To run all test:

```bash
cargo test
```

## Other useful links

* [Implement builder pattern](http://www.ameyalokare.com/rust/2017/11/02/rust-builder-pattern.html)
* [A journey into iterators](https://hoverbear.org/2015/05/02/a-journey-into-iterators/)
